/*
 * @describe: 公共函数库
 * @coder: Porden
 * @Date: 2022-10-21 17:19:20
 * @LastEditTime: 2022-11-09 10:43:57
 */
/**
 * ———————————————————————————————————————————————引入文件————————————————————————————————————————————————————————————
 */
const docx = require('docx-preview')
/**
 * ———————————————————————————————————————————————函数类——————————————————————————————————————————————————————————————
 */
class HookMethods {
  /** 下拉框所有映射数据
     *
     * @param {*} key 对象名
     * @param {*} type 对象对应的键
     * @returns
     */
  initCloum(key, type) {
    const obj = {
      // home项
      wayToCharge: {
        1: '按天收费',
        2: '按小时收费'
      },
      isDelete: {
        0: '未删除',
        1: '删除'
      }
    }
    return obj[key][type]
  }
  /** 主对象中查找某一项
     *
     * @param {*} data 目标数组
     * @param {*} item 搜索值
     * @param {*} key  对应的键
     * @returns 对应检索对象
     */
  // allFindItem(data, item, key) {
  //   if (data.find((element) => element[key] == item) === undefined) return item
  //   else {
  //     return data.find((element) => element[key] == item)
  //   }
  // }
  allFindItem(data, item, key) {
    const result = data.find((element) => element[key] == item) || item
    return result || item
  }
  // refactoringAllFindItem(data, item, key) {
  //   const test = data.find((element) => element[key] === item) ?? item
  //   return test
  // }
  /**
   * 拍平数组
   */
  flattenArray(arr) {
    return arr.reduce((result, item) =>
      result.concat(Array.isArray(item) ? this.flattenArray(item) : item), [])
  }
  /**
     * if()删除
     * @param {*} data 总数组
     * @param {*} item 某一项值
     * @param {*} key 某一项键
     * @param {*} modifyThe 查询到后需要重新修改的值
     * @returns 操作后数组
     */
  delteItem(data, item, key, modifyThe) {
    if (modifyThe === undefined) {
      return data.splice(data.findIndex(data => (data[key] === item)), 1)
    } else {
      const index = data.findIndex(data => (data[key] === item))
      return data[index][key] = modifyThe
    }
  }
  /** 模糊查询
     *
     * @param {*} arr  目标数组
     * @param {*} query 模糊值
     * @param {*} key
     * @returns 返回模糊查询后的数组
     */
  searchAll(arr, query, key = []) {
    // console.log(arr, query)
    return arr.filter((v) =>
      Object.values(
        Object.fromEntries(
          Object.entries(v).filter((item) =>
            key.length ? key.includes(item[0]) : true
          )
        )
      ).some((v) => new RegExp(query + '').test(v))
    )
  }
  /** 替代JSON.parse(JSON.strinfly())
     *
     * @param {*} obj
     * @returns
     */
  deepCopy(data, hash = new WeakMap()) {
    if (typeof data !== 'object' || data === null) {
      throw new TypeError('传入参数不是对象')
    }
    // 判断传入的待拷贝对象的引用是否存在于hash中
    if (hash.has(data)) {
      return hash.get(data)
    }
    const newData = {}
    const dataKeys = Object.keys(data)
    dataKeys.forEach(value => {
      const currentDataValue = data[value]
      // 基本数据类型的值和函数直接赋值拷贝
      if (typeof currentDataValue !== 'object' || currentDataValue === null) {
        newData[value] = currentDataValue
      } else if (Array.isArray(currentDataValue)) {
        // 实现数组的深拷贝
        newData[value] = [...currentDataValue]
      } else if (currentDataValue instanceof Set) {
        // 实现set数据的深拷贝
        newData[value] = new Set([...currentDataValue])
      } else if (currentDataValue instanceof Map) {
        // 实现map数据的深拷贝
        newData[value] = new Map([...currentDataValue])
      } else {
        // 将这个待拷贝对象的引用存于hash中
        hash.set(data, data)
        // 普通对象则递归赋值
        newData[value] = deepCopy(currentDataValue, hash)
      }
    })
    return newData
  }
  /**
   *  cookie操作【set，get，del】
   */
  cookie = {
    set: function(name, value, day) {
      const oDate = new Date()
      oDate.setDate(oDate.getDate() + (day || 30))
      document.cookie = name + '=' + value + ';expires=' + oDate + '; path=/;'
    },
    get: function(name) {
      const str = document.cookie
      const arr = str.split('; ')
      for (let i = 0; i < arr.length; i++) {
        const newArr = arr[i].split('=')
        if (newArr[0] === name) {
          return newArr[1]
        }
      }
    },
    del: function(name) {
      this.set(name, '', -1)
    }
  }
  /**
   * 常用的正则
   */
  checkStr(str, type) { // 常用正则验证，注意type大小写
    switch (type) {
      case 'phone': // 手机号码
        return /^1[3|4|5|6|7|8|9][0-9]{9}$/.test(str)
      case 'tel': // 座机
        return /^(0\d{2,3}-\d{7,8})(-\d{1,4})?$/.test(str)
      case 'card': // 身份证
        return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(str)
      case 'pwd': // 密码以字母开头，长度在6~18之间，只能包含字母、数字和下划线
        return /^[a-zA-Z]\w{5,17}$/.test(str)
      case 'postal': // 邮政编码
        return /[1-9]\d{5}(?!\d)/.test(str)
      case 'QQ': // QQ号
        return /^[1-9][0-9]{4,9}$/.test(str)
      case 'email': // 邮箱
        return /^[\w-]+(.[\w-]+)*@[\w-]+(.[\w-]+)+$/.test(str)
      case 'money': // 金额(小数点2位)
        return /^\d*(?:.\d{0,2})?$/.test(str)
      case 'URL': // 网址
        return '/(http|ftp|https)://[\w-_]+(.[\w-_]+)+([\w-.,@?^=%&:/~+#]*[\w-@?^=%&/~+#])?/'.test(str) // 未测试
      case 'IP': // IP
        return /((?:(?:25[0-5]|2[0-4]\d|[01]?\d?\d)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d?\d))/.test(str)
      case 'date': // 日期时间
        return /^(\d{4})-(\d{2})-(\d{2}) (\d{2})(?::\d{2}|:(\d{2}):(\d{2}))$/.test(str) ||
          /^(\d{4})-(\d{2})-(\d{2})$/.test(str)
      case 'number': // 数字
        return /^[0-9]$/.test(str)
      case 'english': // 英文
        return /^[a-zA-Z]+$/.test(str)
      case 'chinese': // 中文
        return /^[\u4E00-\u9FA5]+$/.test(str)
      case 'lower': // 小写
        return /^[a-z]+$/.test(str)
      case 'upper': // 大写
        return /^[A-Z]+$/.test(str)
      case 'HTML': // HTML标记
        return /<("[^"]*"|'[^']*'|[^'">])*>/.test(str)
      default:
        return true
    }
  }
  /**
   *
   * @param {*} time 2022-10-11T10:40:41.505+08:00   时间格式
   * @return YYYY-mm-dd hh:MM:ss
   */
  dataTimeInit(time, isType = 'All') {
    // var date = new Date(Date.parse(new Date(time.split("T")[0] + " " + time.split("T")[1].split("+")[0]))); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var date = new Date(time) // 时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-'
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-'
    var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' '
    var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':'
    var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':'
    var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
    if (isType === 'All') {
      const setData = Y + M + D + h + m + s
      return setData.trim()
    } else if (isType === 'Y-M') {
      let setData = Y + M
      setData = setData.slice(0, -1)
      return setData.trim()
    } else {
      const setData = Y + M + D
      return setData.trim()
    }
  }

  /**
   * 多少时间前   后
   * @param {*} time
   * @return
   */
  amountOfCalculation(startTime, endTime, type = 'hour') {
    // const sfgz = this.conversion(this.sfglList, item.feestandard_id, 'feestandard_id')
    const timeDifference = new Date(startTime).getTime() - new Date(endTime).getTime()
    if (type === 'hour') {
      return Math.floor(timeDifference / (24 * 3600 * 1000)) === 0 ? 1 : Math.floor(timeDifference / (24 * 3600 * 1000))
    }
  }
  /**
   *  计算倒计时
   * @param {*} startTime 时间戳
   * @param {*} endTime
   * @param {*} type 类型
   * @returns
   */
  amountOfCalculation1(startTime, endTime, time) {
    var nowTime = +new Date(startTime)
    var inputTime = +new Date(endTime)
    if (startTime === '') {
      nowTime = +new Date()
    }
    if (time) {
      inputTime = new Date()
      inputTime.setHours(time.h, time.m, time.s, time.e)
    }

    var remain = (inputTime - nowTime) / 1000
    var day = parseInt(remain / 60 / 60 / 24).toString().replace(/^(\d)$/, '0$1')
    var h = parseInt(remain / 60 / 60 % 24).toString().replace(/^(\d)$/, '0$1')
    var m = parseInt(remain / 60 % 60).toString().replace(/^(\d)$/, '0$1')
    var s = parseInt(remain % 60).toString().replace(/^(\d)$/, '0$1')
    return [day, h, m, s]
  }
  /**
   * start
   */
  /**
 * 从里到外遍历查询每层对应数据
 * @param {*} data 需要查询的树形数据最里层值
 * @param {*} List 需要遍历的树形对象
 * @returns  从外到目标值data的全部匹配项
 */
  selectOption(data, List) {
    if (!data) return
    const valueToSelect = data
    const options = List
    // 递归遍历选项并选中匹配的值
    const selected = this.selectOptionRecursive(options, valueToSelect)
    const arrDepartment = []
    selected.forEach(data => {
      arrDepartment.push(data.value)
    })
    return arrDepartment
  }
  selectOptionRecursive(options = [], valueToSelect) {
    for (let i = 0; i < options.length; i++) {
      const option = options[i]
      if (option.value === valueToSelect) {
        return [option]
      }
      if (option.children) {
        const selected = this.selectOptionRecursive(option.children, valueToSelect)
        if (selected) {
          return [option, ...selected]
        }
      }
    }
    return null
  }
  /**
   * end
   */
  /**
   * @param {*} length 随机数长度
   * @return
   */
  getNum(length, a) {
    let str = ''
    for (let i = 0; i < length; i++) {
      str += String(Math.floor(Math.random() * 10))
    }
    return str
  }
  /**
   * 数组转换为固定格式
   * @param {*} numbers 对应数组
   * @param {*} model 模版
   * @return 对应格式
   * (phoneNumber([1,2,3,4,5,6,7,8,9,0]),(xxx)xxx-xxxx);
   * return (123)456-7890;
   */
  phoneNumber(numbers, model) {
    return model.replace(/x/g, () => numbers.shift() || 'x')
  }
  /**
   * 判断本地是否有该对象并且该对象里面是都有某个属性值
   * @param {*} key  对应的本地存储的对象名
   * @param {*} property 对象里面的key
   * @param {*} typeStorage 本地存储类型
   * @returns
   */
  checkObjectInLocalStorage(key, property, typeStorage = localStorage) {
    const storedData = typeStorage.getItem(key)
    if (storedData) {
      const storedObject = JSON.parse(storedData)
      if (storedObject.hasOwnProperty(property) && storedObject[property] !== null) {
        return true // 对象里有数据
      } else {
        return false // 对象里没有数据
      }
    } else {
      return false // 没有找到对应的对象
    }
  }
  /**
   *
   * @param {*} base64 base64字符串  带文件头
   * @param {*} fileName 文件名
   * @returns
   */
  base64ToFile(base64, fileName) {
    const arr = base64.split(',')
    const mime = arr[0].match(/:(.*?);/)[1] // 文件的类型，base64的文件头    image/png
    const bstr = atob(arr[1])
    let n = bstr.length
    const u8arr = new Uint8Array(n)

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n)
    }
    return new File([u8arr], fileName, { type: mime })
  }
  /**
   * 试用前请下载jspdf和html2canvas
   * @param {*} idName dom id名
   * @return 返回预览界面并弹出打印界面
   */
  exportToPdf(idName) {
    var jsPDF = require('jspdf')
    var html2canvas = require('html2canvas')
    const element = document.getElementById(idName) // 要转换为PDF的dom节点ID名
    html2canvas(element).then(canvas => {
      const imgData = canvas.toDataURL('image/png')
      const doc = new jsPDF.jsPDF()
      const imgProps = doc.getImageProperties(imgData)
      const pdfWidth = doc.internal.pageSize.getWidth()
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width
      doc.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight)
      // 显示预览界面
      doc.autoPrint()
      window.open(doc.output('bloburl'), '_blank')
      // window.print()
    })
  }
  /**
   * 文件类型浏览
   */
  /**
   * pdf浏览
   * @param {二进制流} binaryData
   * @return 浏览文件可直接赋值
   */
  pdfLook(binaryData) {
    const blob = new Blob([binaryData], { type: 'application/pdf' })
    return URL.createObjectURL(blob)
  }
  /**
   * word浏览  在vue中使用必须将wordLook方法使用this.$nextTick 包裹
   * @param {二进制流} binaryData
   * @return 浏览文件可直接赋值
   */
  wordLook(binaryData, html) {
    return docx.renderAsync(binaryData, html) // 渲染到页面预览
  }
  /**
   * 图片浏览
   * @param {二进制流} binaryData
   * @return promise 需要用.then来赋值
   */
  imageLook(binaryData) {
    console.log(binaryData)
    return new Promise((resolve, reject) => {
      const blob = new Blob([binaryData], { type: 'image/jpeg' }) // 类型一定要写！！！
      const reader = new FileReader()
      reader.readAsDataURL(blob)
      reader.onload = () => {
        resolve(reader.result) // 文件base64
      }
    })
  }
  /**
   * @param 下载文件浏览
   * @param {二进制流} binaryData
   * @param {file文件类型} file
   */
  download(binaryData, file) {
    const blob = new Blob([binaryData], {
      type: 'text/plain'
    })
    const fileName = file.name
    // 允许用户在客户端上保存文件
    if (window.navigator.msSaveOrOpenBlob) {
      navigator.msSaveBlob(blob, fileName)
    } else {
      var link = document.createElement('a')
      link.href = window.URL.createObjectURL(blob)
      link.download = fileName
      link.click()
      // 释放内存
      window.URL.revokeObjectURL(link.href)
    }
    return
  }
  /**
   * 文件浏览END
   */
}

export default new HookMethods()
